
#pragma once

#include "iostream"
#include "vector"

namespace cargo{
    class Box {
        int length;
        int width;
        int height;
        double weight;
        int value;

        int boxVolume(Box box){
            return box.length*box.width*box.height;
        }

        Box findBoxWithLessVolume(Box box[], int boxLen){
            Box boxWithLessVolume = box[0];
            for (int i = 1; i < boxLen; ++i) {
                if(boxVolume(box[i]) < boxVolume(boxWithLessVolume)){
                    boxWithLessVolume = box[i];
                }
            }
            return boxWithLessVolume;
        }
    public:

        Box(int length, int width, int height, double weight, int value);

        bool operator==(const Box &box) const;

        bool operator!=(const Box &box) const;

        friend std::ostream &operator<<(std::ostream &os, const Box &box) {
            os << "length: " << box.length << " width: " << box.width << " height: " << box.height << " weight: "
               << box.weight << " value: " << box.value;
            return os;
        }

        friend std::istream &operator>>(std::istream &is, Box &box){
            is >> box.length >> box.width >> box.height >> box.weight >> box.value;
            return is;
        }

        int getLength() const;

        void setLength(int length);

        int getWidth() const;

        void setWidth(int width);

        int getHeight() const;

        void setHeight(int height);

        double getWeight() const;

        void setWeight(double weight);

        int getValue() const;

        void setValue(int value);

        static int totalValue(Box box[], int boxLen);

        static bool isSumLWHLessThan(Box box[], int boxLen, int value);

        static double getMaxWeightUnderVolume(Box box[], int boxLen, int maxV);


    };
};
